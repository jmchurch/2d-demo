# README #

### 2D Assignment ###
- This is a 2D assignment using assets from Kenny.nl

## Assignment Requirements ##
- Bring in a new tileset and create a short level
- Bring in a new sprite sheet and make an animation out of it
- Add a collectable
- Create an enemy that has its own movement. Player can jump on enemy to destory them.

## Completed Tasks ###
- Brought in medieval tileset and created a level
- Brought in a sprite sheet, created idle, move, and jump animations
- Added a spawning system for collectables, updates counter
- Added enemy that deals damage and is killed when jumped on
