using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TorchMovement : MonoBehaviour
{
    private bool rightSide = false;
    private bool leftSide = true;
    private Vector3 originalPos = new Vector3(-10.1400003f,-3.08999991f,-9.44470978f);
    private Vector3 secondPos = new Vector3(4f,-3.08999991f,-9.44470978f);
    private bool inCoroutine = false;

    // Update is called once per frame
    void Update()
    {
        // lerp to the right side if on the right side
        if (leftSide)
        {
            if (!inCoroutine)
            {
                Debug.Log("Hit Left Side");
                inCoroutine = true;
                if (gameObject != null)
                {
                    transform.DOMove(secondPos, 3f);

                }
            }

            if (transform.position == secondPos)
            {
                inCoroutine = false;
                rightSide = true;
                leftSide = false;
            }
        }

        // lerp to left side if on the right side
        if (rightSide)
        {
            if (!inCoroutine)
            {
                Debug.Log("Hit Right Side");
                inCoroutine = true;
                if (gameObject != null)
                {
                    transform.DOMove(originalPos, 3f);
                }
            }

            if (transform.position == originalPos)
            {
                inCoroutine = false;
                rightSide = false;
                leftSide = true;
            }
        }  
    }
}
