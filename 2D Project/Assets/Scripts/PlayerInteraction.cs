using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerInteraction : MonoBehaviour
{
    public Text[] textList;
    private int stickAmount = 0;
    private int health = 10000;

    private void Update()
    {
        if (health <= 0)
        {
            SceneManager.LoadScene("SampleScene");
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Stick"))
        {
            Destroy(other.gameObject);
            stickAmount++;
            textList[0].text = "Sticks gathered: " + stickAmount;
        }

        if (other.CompareTag("DangerousTorch"))
        {
            health -= 25;
            textList[1].text = "Health: " + health;
        }
    }
}
