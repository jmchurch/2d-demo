using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 40f;
    public Transform raycastPos;
    public Animator animator;
    public SpriteRenderer SpriteRenderer;
    
    private bool IsWalking = false;
    private bool IsJumping = false;
    private Rigidbody2D rb;
    private int jumpAmount = 0;
    public int maxJumps = 1;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 move = new Vector2();
        // set animator parameters
        animator.SetBool("IsMoving", IsWalking);
        animator.SetBool("IsJumping", IsJumping);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (jumpAmount > 0)
            {
                IsJumping = true; // set IsJumping to true
                move.y = 300; // apply force updwards
                jumpAmount -= 1; // decrement jump amount
            }
        }


        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            if (Input.GetKey(KeyCode.A))
            {
                SpriteRenderer.flipX = true;
            }
            else
            {
                SpriteRenderer.flipX = false;
            }
            IsWalking = true;
        }
        else
        {
            IsWalking = false;
        }

        move.x = Input.GetAxis("Horizontal");
        // increase speed over time
        rb.AddForce(move * speed);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        // when the player hits the ground
        if (other.collider.CompareTag("Ground"))
        {
            jumpAmount = maxJumps; // reset amount of jumps
            IsJumping = false; // set IsJumping to false
        }
    }
}
