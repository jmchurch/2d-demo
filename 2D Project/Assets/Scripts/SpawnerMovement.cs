using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SpawnerMovement : MonoBehaviour
{
    public GameObject spawnableObject;
    
    private bool rightSide = false;
    private bool leftSide = true;
    private Vector3 originalPos = new Vector3(-9.7f, 2f, -0.7f);
    private Vector3 secondPos = new Vector3(4f, 2f, -0.7f);
    private bool inCoroutine = false;
    private bool spawned = false;

    public float speed = 5;
    void Update()
    {
        // lerp to the right side if on the right side
        if (leftSide)
        {
            if (!inCoroutine)
            {
                Debug.Log("Hit Left Side");
                inCoroutine = true;
                transform.DOMove(secondPos, 3f);
            }
            if (transform.position == secondPos)
            {
                inCoroutine = false;
                rightSide = true;
                leftSide = false;
            }
        }

        // lerp to left side if on the right side
        if (rightSide)
        {
            if (!inCoroutine)
            {
                Debug.Log("Hit Right Side");
                inCoroutine = true;
                transform.DOMove(originalPos, 3f);
            }
            if (transform.position == originalPos)
            {
                inCoroutine = false;
                rightSide = false;
                leftSide = true;
            }
        }

        if (!spawned)
        {
            spawned = true;
            StartCoroutine(SpawnObject());
        }
    }

    IEnumerator SpawnObject()
    {
        yield return new WaitForSeconds(.1f);
        float x = Random.Range(1, 5);
        float y = Random.Range(1, 5);
        GameObject obj = Instantiate(spawnableObject, transform.position, Quaternion.identity);
        obj.GetComponent<Rigidbody2D>().AddForce(new Vector2(x,y));
        spawned = false;
    }
    
}
